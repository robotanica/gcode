G28 ; Home. Keep only for standalone testing purposes

M107 ; Fan off
M104 S0 ; Turn off extruder
M140 S0 ; Turn off bed

; M190 R25 ; Wait until bed temperature reaches 30c

G0 X110 Y210 Z50 F3000 ; Move up and back

G0 X110 Y210 Z1 F3000 ; Lower
G0 X110 Y1 Z1 F2400 ; Remove print
G0 X110 Y30 Z1 F8000 ; Shake it out
G0 X110 Y1 Z1 F8000 ; Shake it out
G0 X110 Y195 Z1 F8000 ; Retract

; M106 S0 ; Fan off
; M104 S0 ; Turn off extruder
; M140 S0 ; Turn off bed
; M84 ; Disable motors
